/* pingpress daemon */

#define _GNU_SOURCE

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <netinet/in.h>

#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct thstore {
	char *address;
	int result;
};

static int is_host_alive(char *);
static void *thmain(void *);
static int pingpress(char **, char *);
static int recv_first_line(int, char **);
static int count_hosts(char *);
static int parse_req(char *, char ***);
static char *get_http_date_str(void);
static int send_http_response(char *, char *, int);
static int send_response_200_ok(int, char *);
static int handle_connection(int);
static void print_usage(char *);
static int open_port(int);
static void main_loop(int);
static void sighandler(int signo);
static void bind_sighandler(void);
static void daemonize(char *);
static void parse_args(int, char **, int *, int *, char **);
static void write_pid_to_file(char *);

int
is_host_alive(char *host_ip)
{
	pid_t pid;
	int status;

	if ((pid = fork()) < 0) {
		return -1;
	} else if (pid == 0) {
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
		execlp("ping", "ping", "-c", "1", host_ip, NULL);
		exit(EXIT_FAILURE);
	}
	waitpid(pid, &status, 0);
	if (WIFEXITED(status))
		return WEXITSTATUS(status) == 0;
	return -1;
}

void *
thmain(void *arg)
{
	struct thstore *store = (struct thstore *) arg;

	switch (is_host_alive(store->address)) {
	case 0:
		store->result = '0';
		break;
	case 1:
		store->result = '1';
		break;
	case -1:
		store->result = 'x';
		break;
	}
	return NULL;
}

int
pingpress(char **hosts, char *result)
{
	int i = 0, host_c = 0, rv = 1;
	struct thstore *stores = NULL;
	pthread_t *threads = NULL;

	while (hosts[host_c])
		host_c++;
	if (!(threads = (pthread_t *) calloc(host_c, sizeof(pthread_t)))
			|| !(stores = (struct thstore *)
				calloc(host_c, sizeof(struct thstore))))
		goto pingpress_err;
	for (i = 0; i < host_c; i++) {
		stores[i].address = hosts[i];
		pthread_create(threads + i, NULL, thmain, stores + i);
	}
	for (i = 0; i < host_c; i++) {
		pthread_join(threads[i], NULL);
		result[i] = stores[i].result;
	}
	rv = 0;
pingpress_err:
	free(stores);
	free(threads);
	return rv;
}

int
recv_first_line(int sockfd, char **result)
{
	int buff_len = 512, llen = 0, i;
	ssize_t bs_read;
	char *line = NULL, *buff = (char *) calloc(buff_len, sizeof(char));

	if (!buff)
		goto recv_first_line_err;
	for (;;) {
		bs_read = recv(sockfd, buff, sizeof(char) * buff_len, 0);
		if (bs_read <= 0)
			goto recv_first_line_err;
		line = realloc(line, sizeof(char) * (llen + bs_read));
		if (!line)
			goto recv_first_line_err;
		for (i = 0; i < bs_read; i++) {
			if (buff[i] == '\n' || buff[i] == '\r') {
				line[llen + i] = '\0';
				goto recv_first_line_done;
			}
			line[llen + i] = buff[i];
		}
		llen += bs_read;
	}
recv_first_line_done:
	free(buff);
	*result = line;
	return 0;
recv_first_line_err:
	free(buff);
	free(line);
	return 1;
}

int
count_hosts(char *req)
{
	int i = 0, host_c = 0;

	while (req[i] != ' ') {
		host_c++;
		while (req[i] != ' ' && req[i] != '&')
			i++;
		if (req[i] == '&')
			i++;
	}
	return host_c;
}

int
parse_req(char *req, char ***result)
{
	int i = 0, h = 0, h_len, host_c;
	char **hosts, *host;

	if (strlen(req) <= strlen("GET / HTTP/1.1"))
		return 2; /* No request */
	if (strncmp(req, "GET /", sizeof(char) * 5) != 0)
		return 3; /* Wrong method */
	req += 5;
	host_c = count_hosts(req);
	if (!(hosts = (char **) calloc(host_c + 1, sizeof(char *))))
		return 1; /* Internal error */
	hosts[host_c] = NULL;
	while (req[i] != ' ') {
		if (req[i] == '&') {
			i++;
			continue;
		}
		for (h_len = 0; req[i + h_len] != ' ' && req[i + h_len] != '&';
			h_len++);
		if (!(host = (char *) calloc(h_len + 1, sizeof(char))))
			goto parse_req_err;
		strncpy(host, req + i, sizeof(char) * h_len);
		host[h_len] = '\0';
		hosts[h++] = host;
		i += h_len;
	}
	*result = hosts;
	return 0; /* Ok */
parse_req_err:
	if (hosts) {
		for (i = 0; i < host_c; i++)
			free(hosts[i]);
		free(hosts);
	}
	return 1; /* Internal error */
}

char *
get_http_date_str(void)
{
	/* HTTP protocol uses date in RFC822 format, which has been modified by
	 * RFC1123 (YYYY instead of YY). Datetime is of fixed length */
	char *rfc1123_date = "Sun, 06 Nov 1994 08:49:37 GMT\n";
	size_t rfc1123_date_len = strlen(rfc1123_date);
	char *date = calloc(rfc1123_date_len + 1, sizeof(char));
	if (!date)
		return NULL;
	time_t now = time(0);
	if (!strftime(date, rfc1123_date_len + 1, "%a, %d %b %Y %H:%M:%S %Z",
			gmtime(&now)))
		return NULL;
	return date;
}

int
send_response_200_ok(int sockfd, char *result)
{
	char *res = NULL;
	int res_c = 0, i, rc = 1;
	
	while (result[res_c])
		res_c++;
	res = (char *) calloc(2 * res_c, sizeof(char));
	if (!res)
		goto send_response_200_exit;
	for (i = 0; i < res_c; i++) {
		res[2 * i] = result[i];
		res[2 * i + 1] = ' ';
	}
	res[2 * res_c - 1] = '\0'; /* strip trailing whitespace */

	rc = send_http_response("200 OK", res, sockfd);
send_response_200_exit:
	free(res);
	return rc;
}

int
send_http_response(char *status_code, char *content, int sockfd)
{
	char *date = NULL, *response = NULL;
	char *fmt = "HTTP/1.1 %s\n"
		"Server: pingpress\n"
		"Date: %s\n"
		"Content-type: text/plain\n"
		"Content-Length: %d\n"
		"Last-Modified: %s\n"
		"Connection: close\n"
		"\n"
		"%s";
	int rc = 1;

	if (!(date = get_http_date_str()))
		goto send_http_response_exit;
	if (!asprintf(&response, fmt, status_code, date, strlen(content), date,
			content))
		goto send_http_response_exit;
	rc = send(sockfd, response, strlen(response), 0) == -1;
send_http_response_exit:
	free(date);
	free(response);
	return rc;
}

int
handle_connection(int sockfd)
{
	int i = 0, rc = 1;
	char **hosts, *req = NULL, *result = NULL;

	if (recv_first_line(sockfd, &req) != 0 || !req) {
		send_http_response("500 Internal Server Error", "", sockfd);
		goto handle_connection_exit;
	}
	switch (parse_req(req, &hosts)) {
	case 1:
		rc = send_http_response("500 Internal Server Error", "", sockfd);
		goto handle_connection_exit;
	case 2:
		rc = send_http_response("204 No Content", "", sockfd);
		goto handle_connection_exit;
	case 3:
		rc = send_http_response("405 Method Not Allowed", "", sockfd);
		goto handle_connection_exit;
	}
	if (!hosts) {
		rc = send_http_response("204 No Content", "", sockfd);
		goto handle_connection_exit;
	}
	while (hosts[i])
		i++;
	result = (char *) calloc(i + 1, sizeof(char));
	if (!result) {
		send_http_response("500 Internal Server Error", "", sockfd);
		goto handle_connection_exit;
	}
	if (pingpress(hosts, result)) {
		send_http_response("500 Internal Server Error", "", sockfd);
		goto handle_connection_exit;
	}
	rc = send_response_200_ok(sockfd, result);
handle_connection_exit:
	free(result);
	free(req);
	if (hosts) {
		for (i = 0; hosts[i]; i++)
			free(hosts[i]);
		free(hosts);
	}
	shutdown(sockfd, SHUT_RDWR);
	close(sockfd);
	return rc;
}

void
print_usage(char *prgname)
{
	fprintf(stderr, "%s 1.0 (c) 2013-2014 Martin Kopta\n", prgname);
	fprintf(stderr, "Usage: %s  [-d] [-l port] [-p pidfile] [-v]\n", prgname);
}

int
open_port(int port)
{
	int sockfd;
	struct sockaddr_in address;

	if ((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
		goto open_port_error;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	if (bind(sockfd, (struct sockaddr *) &address, sizeof(address)) == -1
			|| listen(sockfd, 0) == -1)
		goto open_port_error;
	return sockfd;
open_port_error:
	/* close fd? */
	fprintf(stderr, "Cannot open port %d.\n", port);
	exit(EXIT_FAILURE);
}

void
main_loop(int sockfd)
{
	pid_t pid;
	int c_sockfd;
	struct sockaddr_in rem_addr;
	unsigned int rem_addr_length;

	for (;;) {
	      rem_addr_length = sizeof(rem_addr);
	      if ((c_sockfd = accept(sockfd, (struct sockaddr *) &rem_addr,
			      &rem_addr_length)) == -1) {
		      fprintf(stderr, "Failed to accept connection.\n");
		      continue;
	      }
	      if ((pid = fork()) == 0) {
		      if (handle_connection(c_sockfd)) {
			      fprintf(stderr, "Failed to handle connection\n");
			      exit(EXIT_FAILURE);
		      }
		      exit(EXIT_SUCCESS);
	      } else if (pid < 0) {
		      fprintf(stderr, "Failed to fork.\n");
		      continue;
	      }
	}
}

void
daemonize(char *pidfile)
{
	pid_t pid = fork();

	if (pid < 0) {
		fprintf(stderr, "Failed to fork().\n");
		exit(EXIT_FAILURE);
	} else if (pid > 0) {
		exit(EXIT_SUCCESS);
	}
	if (pidfile)
		write_pid_to_file(pidfile);
	(void) umask(0777);
	if (setsid() < 0
			|| chdir("/") < 0
			|| close(STDIN_FILENO) < 0
			|| close(STDOUT_FILENO) < 0
			|| close(STDERR_FILENO) < 0) {
		fprintf(stderr, "Failed to daemonize.\n");
		exit(EXIT_FAILURE);
	}
}

void
write_pid_to_file(char *fname)
{
	FILE *fp;

	if (!(fp = fopen(fname, "w+"))
			|| fprintf(fp, "%d\n", (int) getpid()) < 0
			|| fclose(fp) == EOF)
		goto write_pid_to_file_error;
	return;
write_pid_to_file_error:
	(void) fclose(fp);
	fprintf(stderr, "Failed to write pid to file '%s'.\n", fname);
	exit(EXIT_FAILURE);
}

void
parse_args(int argc, char **argv, int *daemon, int *port, char **pidfile)
{
	int opt;

	while ((opt = getopt(argc, argv, "dl:p:v")) != -1) {
		switch (opt) {
		case 'd':
			*daemon = 1;
			break;
		case 'l':
			*port = atoi(optarg);
			break;
		case 'p':
			*pidfile = optarg;
			break;
		case 'v':
			print_usage(*argv);
			exit(EXIT_SUCCESS);
		default:
			print_usage(*argv);
			exit(EXIT_FAILURE);
		}
	}
	if (*port < 0 || *port > 65535) {
		fprintf(stderr, "Port out of range <0-65535>.\n");
		print_usage(*argv);
		exit(EXIT_FAILURE);
	}
}

void
sighandler(int signo)
{
	/* TODO */
	exit(EXIT_SUCCESS);
}

void
bind_sighandler(void)
{
	if (signal(SIGINT, sighandler) == SIG_ERR) {
		fputs("An error occurred while setting a signal handler.\n", stderr);
		exit(EXIT_FAILURE);
	}
}

int
main(int argc, char **argv)
{
	int fd, port = 8080, daemon = 0;
	char *pidfile = NULL;

	parse_args(argc, argv, &daemon, &port, &pidfile);
	bind_sighandler();
	fd = open_port(port);
	if (daemon) {
		daemonize(pidfile);
	} else if (pidfile) {
		write_pid_to_file(pidfile);
	}
	main_loop(fd);
	return EXIT_SUCCESS;
}
