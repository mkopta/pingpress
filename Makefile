#CFLAGS=-Wall -Wextra -pedantic -ansi -std=c99
LIBS=-lpthread
PREFIX=/usr/local/bin

all: pingpress

pingpress: pingpress.c
	$(CC) $(CFLAGS) $(LIBS) -o pingpress pingpress.c

install: pingpress
	mkdir -p $(PREFIX)
	cp pingpress $(PREFIX)

uninstall:
	rm -f $(PREFIX)/pingpress

clean:
	rm -f pingpress
